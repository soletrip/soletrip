import threading
import os,json,time,sys
from django.http.response import HttpResponse
from django.template.base import Template
from django.template.context import Context

class SystemLoader(threading.Thread):
    __systemLoader = None
    subSystemFolder = os.path.join(os.getcwd(),'subsystem')
    subSystemDict = {}
    @staticmethod
    def getLoader():
        if not SystemLoader.__systemLoader:
            SystemLoader.__systemLoader = SystemLoader()
        return SystemLoader.__systemLoader
        
    @staticmethod
    def initLoader():
        if not SystemLoader.__systemLoader:
            SystemLoader.__systemLoader = SystemLoader()
        SystemLoader.__systemLoader.start()
    
    def run(self):
        threading.Thread.run(self)
        while True:
            for subsys in os.listdir(self.subSystemFolder):
                configJson = os.path.join(self.subSystemFolder,subsys,'config.json')
                if not os.path.exists(configJson):
                    continue
                f = open(configJson)
                configdict = json.load(f)
                f.close()
                self.subSystemDict[configdict['url']] = configdict
            print 'sub sytem : ',[self.subSystemDict]
            time.sleep(60)
            
    def getSubSystemResponse(self,req,url):
        response = None
        print "getSubSystemResponse",self.subSystemDict,self.subSystemDict.keys(),url
        if url in self.subSystemDict.keys():
            sloader = self.subSystemDict.get(url)
            spath = sloader['rander_pkg']
            pathx = os.getcwd()
            for fpath in spath.split('.'):
                pathx = os.path.join(pathx,fpath)
            sys.path.append(pathx)
            subsys = __import__(sloader['rander_py'])
            reload(subsys)
            subsysCls = getattr(subsys, sloader['rander_cls'])
            ssc  = subsysCls()
            response = ssc.index(req)
        if response:
            return response
        else:
            pass#return HttpResponse('Sub sytem not found!')

SystemLoader.initLoader()

def index2(req):
    t = Template('My name is {{ name }}.')
    c = Context({'name':'django'})
    return t.render(c)

def index(req,urlstr):
    print "the url is : ",urlstr,urlstr == 'logon'
    return SystemLoader.getLoader().getSubSystemResponse(req,urlstr)
