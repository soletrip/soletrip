from mongoengine import *

from SoleTrip.settings import MONGODB
from subsystem.Plans.User.models import User
from subsystem.Plans.Billing.moodels import Billing
from subsystem.Plans.models import Plan

connect(MONGODB)

class Product(Document):
    start_date = DateTimeField()
    end_date = DateTimeField()
    days = IntField()
    plan = ReferenceField(Plan)
    attendees = ListField(ReferenceField(User))
    billing = ReferenceField(Billing)

