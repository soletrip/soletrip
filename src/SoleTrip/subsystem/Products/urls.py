__author__ = 'pp'

from django.conf.urls import include, url
from subsystem.Products.views import index, detail

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^(?P<product_id>[0-9]+)/$', detail, name='detail')
]