from django.http.response import HttpResponse
from pymongo import MongoClient
import pymongo

def index(req):
    client = MongoClient()
    db = client.test
    cursor = db.restaurants.find({"grades.score": {'$gt': 30}, "cuisine": "Italian"}).sort(
        [
            ("borough", pymongo.ASCENDING),
            ("zipcode", pymongo.DESCENDING)
        ]
    )
    # for doc in cursor:
    #     print(doc)
    return HttpResponse("Product hello~" + str(cursor.count()))

def detail(req, product_id):
    return HttpResponse('Products Details ...' + product_id)