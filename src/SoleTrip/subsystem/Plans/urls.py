__author__ = 'pp'

from django.conf.urls import include, url
from subsystem.Plans.views import index, detail

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^(?P<plan_id>[0-9]+)/$', detail, name='detail')
]