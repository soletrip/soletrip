from mongoengine import *

from SoleTrip.settings import MONGODB
from subsystem.Plans.Location.models import Scenic, Hotel, City
from subsystem.Plans.Traffic.models import Traffic
from subsystem.Plans.User.models import User

connect(MONGODB)

class Journey(EmbeddedDocument):
    scenic = ReferenceField(Scenic)
    period = IntField()
    amount = FloatField()
    # transits = ListField() Do not need yet

class Path(EmbeddedDocument):
    city = ReferenceField(City)
    transportations = EmbeddedDocumentListField(Traffic)

class DayPlan(EmbeddedDocument):
    path = EmbeddedDocumentListField(Path)
    journeys = EmbeddedDocumentListField(Journey)
    play_hours = IntField() # hours of touring of a day
    accommodation = ReferenceField(Hotel)
    day_of_tour = IntField() # the day of tour, 1 means day 1 of tour

class Plan(Document):
    # Use Reference better than embedded ?
    # day_plans = EmbeddedDocumentListField(DayPlan)
    day_plans = SortedListField(ReferenceField(DayPlan))
    days = IntField()
    cities = ListField(StringField())

