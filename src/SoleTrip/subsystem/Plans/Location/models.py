from mongoengine import *
from SoleTrip.settings import MONGODB
connect(MONGODB)

class Continent(EmbeddedDocument):
    name = StringField(required=True)
    code = IntField()

class Country(EmbeddedDocument):
    name = StringField(required=True)
    code = IntField(max_length=16)
    continent = EmbeddedDocumentField(Continent)

class Location(EmbeddedDocument):
    latitude = FloatField(required=True)
    longitude = FloatField(required=True)

class City(Document):
    name = StringField(required=True)
    code = StringField(max_length=16)
    country = EmbeddedDocumentField(Country)

class Address(Document):
    location = EmbeddedDocumentField(Location)
    city = ReferenceField(City)
    zipcode = StringField(16)
    district = StringField(32)
    street = StringField(64)
    building = StringField(64)


class Scenic(Document):
    name = StringField(required=True, max_length=32)
    address = ReferenceField(Address)
    # city = ReferenceField(City)
    city = StringField(64)
    overview = StringField(256)
    introduction = StringField(256)
    business_hours = FloatField()
    notice = StringField(128)
    phone_num = StringField(20)
    website = StringField(128)
    grade_index = IntField()
    grade_description = StringField(128)
    hot = IntField() # hot is ranked by numbers
    spot_type = IntField()

class Hotel(Document):
    name = StringField(required=True, max_length=32)
    address = ReferenceField(Address)
    # city = ReferenceField(City)
    city = StringField(64)
    overview = StringField(256)
    introduction = StringField(256)
    business_hours = FloatField()
    notice = StringField(128)
    phone_num = StringField(20)
    website = StringField(128)
    grade_index = IntField()
    grade_description = StringField(128)
    hot = IntField() # hot is ranked by numbers
    spot_type = IntField()