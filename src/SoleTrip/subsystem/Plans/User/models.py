from mongoengine import *
from SoleTrip.settings import MONGODB
from subsystem.Plans.Location.models import Country
connect(MONGODB)

class Company(Document):
    name = StringField(max_length=64)
    phone = StringField(max_length=16)
    address = StringField(max_length=64)

class User(Document):
    email = StringField(max_length=50)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    username = StringField(max_length=50)
    company = ReferenceField(Company)
    mobile_phone = StringField(max_length=16, required=True)

class Tourist(User):
    country = ReferenceField(Country)
    identity = StringField(max_length=32) # usually personal identity number
    passport = StringField(max_length=32) # passport number

class Guide(User):
    country = ReferenceField(Country)
    type = StringField()
    identity = StringField(max_length=32)
    passport = StringField(max_length=32)
    license = StringField(max_length=32)
    note = StringField(140)

class Driver(User):
    country = ReferenceField(Country)
    type = StringField()
    identity = StringField(max_length=32)
    passport = StringField(max_length=32)
    license = StringField(max_length=32)
    note = StringField(140)
