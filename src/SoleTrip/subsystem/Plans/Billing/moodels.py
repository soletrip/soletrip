from mongoengine import *
from SoleTrip.settings import MONGODB

connect(MONGODB)

CURRENCY_TYPE = (
    'CNY',
    'US',
    'AUS',
    'GBP',
)

class Fare(EmbeddedDocument):
    currency_type = StringField(max_length='3', choices=CURRENCY_TYPE)
    currency_to_us = FloatField()
    currency_to_cn = FloatField()
    amount = FloatField()

class Billing(Document):
    traffic_fee = EmbeddedDocumentField(Fare)
    accommodation_fee = EmbeddedDocumentField(Fare)
    scenic_fee = EmbeddedDocumentField(Fare)
    guide_fee = EmbeddedDocumentField(Fare)
    amount = FloatField()
