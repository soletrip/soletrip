from mongoengine import *
from SoleTrip.settings import MONGODB
from subsystem.Plans.User.models import Company


connect(MONGODB)

VEHICLE_SPEC = (
    ('Car', '< 5'),
    ('Mini', '< 7'),
    ('Mid', '< 15'),
    ('Normal', '< 25'),
)

# duration for vehicle usage
VEHICLE_DURATION = ('half-day', 'day', 'none')

# V: Vehicle, F: Flight, T: Train, F: Ferry
TRAFFIC_TYPE = ('V', 'F', 'T', 'F')

class Vehicle(Document):
    company = ReferenceField(Company)
    spec = StringField(choices=VEHICLE_SPEC)
    duration = StringField(choices=VEHICLE_DURATION)


class Flight(Document):
    flight_no = StringField(required=True)


class Traffic(EmbeddedDocument):
    loader = GenericReferenceField()
    type = StringField(max_length=1, choices=TRAFFIC_TYPE)