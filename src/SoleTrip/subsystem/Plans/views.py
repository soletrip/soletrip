from django.http.response import HttpResponse
from pymongo import MongoClient
import pymongo

def index(req):
    client = MongoClient()
    db = client.test
    cursor = db.restaurants.find({"grades.score": {'$gt': 30}, "cuisine": "Italian"}).sort(
        [
            ("borough", pymongo.ASCENDING),
            ("zipcode", pymongo.DESCENDING)
        ]
    )
    # for doc in cursor:
    #     print(doc)
    return HttpResponse("Plans hello~" + str(cursor.count()))

def detail(req, plan_id):
    return HttpResponse('Plans Details ...' + plan_id)