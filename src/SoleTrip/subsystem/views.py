from django.template.base import Template
from django.template.context import Context
from django.http.response import HttpResponse

def index2(req):
    t = Template('My name is {{ name }}.')
    c = Context({'name':'django'}) 
    return HttpResponse(t.render(c))

def index(req):
    return HttpResponse("Hello Home Page~~~~!")
